Nicklas Kenyon
09/12/2016
CS242 - Assignment 1

The TestDie class rolls the die a large number of times and records the number of 
times numbers are rolled and their measured probabilities.
There are two methods for printing out float arrays with formatting. These were used earlier in testing
but are no longer in use. The methods were instead replaced with specific methods for
printing the "counts" array and "probabilities" array with prettier formatting.

In testing, you can specify the number of rolls you would like to perform by changing 
the value of "numberOfTimesToRoll" in its declaration. I had it set to 1000 for my testing.
The expected probability of each face being rolled for a 6-sided die is 1/6 or 0.1667.
This reflects an unweighted die with equal probabilities for each side.
In my results, the measured probability of each number being rolled was typically within 0.0100 of 0.1667,
which I feel means my results represent true probability. I also tested the default constructor and
constructor that allows the specification of the number of sides the die should have.

I did not test the "hasBeenRolled" method as it was not required for the assignment.
I merely included it because it made me feel like the class was more future-ready.

The Die class has the following fields:

- (visibility [optional], binding [optional], type or class) name : description
	- uses within the class

- (private, int) numSides : the number of sides on the die
	- used to specify the range for the random number generator

- (private, int) topFace : if the die were laying on a table,
	this side would be facing up
	
- (private, static, Random) rng : the random number generator that is used when rolling the die

- (private, boolean) hasRolled : keeps track of whether or not the die has been rolled
	since it was last checked
	
The Die class has the following constructors:
Note: rng is static and is instantiated at declaration

public Die() : sets the default values for numSides (6), topFace (6), and hasRolled (false)

public Die(int numberOfSides) : sets numSides and topFace to numberOfSides. Sets hasRolled to default (false)
Note: I did not call the default constructor in the overloaded constructor to reduce uneccessary function calls.

The Die class has the following methods:

- (visibility [optional], binding [optional], return type or Class) name (arguments) : description

- (public, int) getTop () : returns the side that is currently facing up. In other words,
	it returns the field topFace.
	
- (public, boolean) hasBeenRolled () : returns whether or not the die has been rolled since it was last checked

- (public, void) roll () : rolls the die using rng to generate a random number between 1 and numSides, inclusive.
	That number is assigned to topFace and hasRolled is set to true.
	
- (public, int) getNumberOfSides () : returns the number of sides on the die.
