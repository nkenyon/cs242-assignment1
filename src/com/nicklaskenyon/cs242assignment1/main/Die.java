package com.nicklaskenyon.cs242assignment1.main;
import java.util.Random;

/** 
 * @author Nicklas Kenyon 09/12/2016
 * 
 * This class implements a die with a variable amount of sides (by default it is a 6-sided die)
 * and the functionality to roll the die and get the face that is currently facing "up"
 * as if it were laying on a table.
 * 
 */

public class Die {
	/**
	 * number of sides on the die
	 */
	private int numSides;
	/**
	 * if the die were laying on a table, this side would be facing up
	 */
	private int topFace;
	/**
	 * the random number generator that is used when rolling the die
	 */
	private static Random rng = new Random();
	/**
	 * keeps track of whether or not the die has been rolled since it was last checked
	 */
	private boolean hasRolled;
	
	/**
	 * Default constructor for the die class. A default die with 6 sides with the #6 side facing up
	 * is created.
	 */
	public Die(){
		this.numSides = 6;
		this.topFace  = 6;
		this.hasRolled = false;
	}
	
	/**
	 * Constructor that allows you to specify the number of sides the die should have
	 * @param numberOfSides the number of the sides the dice should have (ex. 6, 10, 12, 20)
	 */
	public Die(int numberOfSides){
		this.numSides = numberOfSides;
		this.topFace  = numberOfSides;
		this.hasRolled = false;
	}
	
	/**
	 * This method returns the face on the die that is currently "facing up"
	 * as if it were laying on a table. It also resets the hasRolled field to false.
	 * 
	 * @return the side of the die currently facing up (this.topFace)
	 */
	public int getTop(){
		this.hasRolled = false;
		return this.topFace;
	}
	
	/**
	 * This method returns whether or not the die has been rolled since it was last checked.
	 * If the die hasn't been rolled since it was checked (using the getTop() method)
	 * then it will return false. Otherwise it will return true.
	 * 
	 * @return whether or not the die has been rolled since it was last checked (this.hasRolled)
	 */
	public boolean hasBeenRolled(){
		return this.hasRolled;
	}
	
	/**
	 * This method rolls the die. A random value within the range [1, numSides]
	 * is assigned to the topFace field. The hasRolled field is set to true.
	 */
	public void roll(){
		this.topFace = rng.nextInt(this.numSides)+1;
		this.hasRolled = true;
	}
	
	/**
	 * This method gives the number of sides on the die.
	 * @return number of sides on the die (this.numSides)
	 */
	public int getNumberOfSides(){
		return this.numSides;
	}

}
