package com.nicklaskenyon.cs242assignment1.main;

public class TestDie {
	
	public static void main(String[] args) {
		
		float[] probabilities;
		float[] counts;
		
		final int sides = 6;
		final int numberOfTimesToRoll = 1000;
		
		probabilities = new float[sides];
		counts = new float[sides];
		
		Die die = new Die();
		
		// Check the default die
		System.out.println("Die after construction, getTop: " + die.getTop());
		System.out.println("Die after construction, getNumberOfSides: " + die.getNumberOfSides());
		
		die = new Die(sides);
		
		// Check the die with specified number of sides
		System.out.println("Die after construction, getTop: " + die.getTop());
		System.out.println("Die after construction, getNumberOfSides: " + die.getNumberOfSides());
		
		int result = 0;
		//roll the die multiple times, recording the results in the "counts" array
		for(int i = 0; i < numberOfTimesToRoll; i++){
			die.roll();
			result = die.getTop();
			counts[result-1]++;
		}
		
		//use the "counts" array to calculate the measured probabilities of each face of the die
		for(int i = 0; i < sides; i++){
			probabilities[i] = counts[i] / numberOfTimesToRoll;
		}
		printCounts(counts);
		printProbabilities(probabilities);

	}
	
	// method for generically printing a float array without a name
	public static void printFloatArray(float[] array, int amount){
		System.out.println("\nPrinting " + amount + " elements of the array");
		for(int i = 0; i < amount; i++){
			System.out.print(array[i] + ", ");
		}
		System.out.println("\nPrinted " + amount + " elements of the array");
	}
	
	// method for generically printing a float array with a name
	public static void printFloatArray(float[] array, int amount, String name){
		System.out.println("\nPrinting " + amount + " elements of the array named: " + name);
		for(int i = 0; i < amount; i++){
			System.out.print(array[i] + ", ");
		}
		System.out.println("\nPrinted " + amount + " elements of the array named: " + name);
	}
	
	// method for printing the "counts" array with pretty formatting
	public static void printCounts(float [] array){
		System.out.println("\nPrinting die face counts");
		int total = 0;
		for(int i = 0; i < array.length; i++){
			System.out.println("Face " + (i+1) + " was rolled " + array[i] + " times");
			total+= array[i];
		}
		
		System.out.println("The die was rolled a total of " + total + " times");
	}
	
	// method for printing the "probabilities" array with pretty formatting
	public static void printProbabilities(float[] array){
		System.out.println("\nPrinting measured probabilities");
		float total = 0;
		for(int i = 0; i < array.length; i++){
			System.out.println("Face " + (i+1) + ": " + (array[i] * 100) + "%");
			total += (array[i] * 100);
		}
		
		System.out.println("Total: " + total);
	}

}
